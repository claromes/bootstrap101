# Bootstrap 101

Layout, Components and Utilities. Each watched video is represented by a [tag](https://gitlab.com/claromes/bootstrap101/-/tags), versioning the learned content.

## References

[Playlist](https://www.youtube.com/playlist?list=PLR8OzKI52ppXTZNlOJWDA814gMRkLJCJG) (pt-br) | [Framework Documentation](https://getbootstrap.com/docs/4.3/getting-started/introduction/) | [Cheat Sheet](https://hackerthemes.com/bootstrap-cheatsheet/)
